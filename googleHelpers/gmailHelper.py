# pylint: disable=no-member

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
import base64
from bs4 import BeautifulSoup
from googleHelpers.authHelper import authenticate
from datetime import datetime


def getShifts():

    message = getMostRecentMail()

    # Retrieve message body
    payld = message['payload']

    msg_parts = payld['parts']
    part_data = msg_parts[0]['body']['data']
    # decoding from Base64 to UTF-8
    clean_one = part_data.replace("-", "+").replace("_", "/")
    # decoding from Base64 to UTF-8
    clean_two = base64.b64decode(bytes(clean_one, 'UTF-8'))
    soup = BeautifulSoup(clean_two, "lxml")

    table = soup.find_all("table")[-4]
    rows = table.find_all('tr')

    shifts = []

    for row in rows:
        cols = row.find_all("td")
        date = cols[0].contents[0]
        time = cols[2].contents[0]
        shift = {"Date": date, "Time": time}
        shifts.append(shift)

    return shifts


def getLastUpdatedDate():
    message = getMostRecentMail()
    subject = getHeader(message, "Subject")
    dateStr = subject.split(" ")[-1]
    lastUpdated = datetime.strptime(dateStr, "%d/%m/%y")
    return lastUpdated


def getMostRecentMail():

    creds = authenticate()

    service = build('gmail', 'v1', credentials=creds)

    # Call the Gmail API
    results = service.users().messages().list(
        userId="me", q="from:noreply@synergysuite.com").execute()

    messageId = results["messages"][0]["id"]

    message = service.users().messages().get(
        userId="me", id=messageId).execute()

    return message


def getHeader(msg, requestedHeader):
    headers = msg['payload']['headers']
    for header in headers:
        if header['name'] == requestedHeader:
            return header['value']


if __name__ == '__main__':
    print(getShifts())
