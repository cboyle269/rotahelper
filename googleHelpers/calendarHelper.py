# pylint: disable=no-member

from googleHelpers.authHelper import authenticate
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from datetime import datetime
import copy


def createShifts(shifts):
    creds = authenticate()

    service = build("calendar", "v3", credentials=creds)

    eventTemplate = {
        'summary': 'Work - Eddie Rockets',
        'description': 'Test event created by rotaHelper',
        'start': {
            'dateTime': 'PLACEHOLDER',
            'timeZone': 'Europe/London',
        },
        'end': {
            'dateTime': 'PLACEHOLDER',
            'timeZone': 'Europe/London',
        },
        'reminders': {
            'useDefault': True
        },
    }

    for shift in shifts:
        startTimeStr = shift['Time'].split("-")[0]
        endTimeStr = shift['Time'].split("-")[1]
        dateStr = shift['Date']
        event = copy.deepcopy(eventTemplate)
        event['start']['dateTime'] = parseDateTime(
            dateStr, startTimeStr)
        event['end']['dateTime'] = parseDateTime(
            dateStr, endTimeStr)

        event = service.events().insert(calendarId='primary', body=event).execute()

        print("Shift created for ", dateStr)


def parseDateTime(dateStr, timeStr):
    # Add leading 0s if needed
    if(timeStr.find(":") == -1 and len(timeStr) != 2):
        timeStr = "0"+timeStr
    if(timeStr.find(":") != -1 and len(timeStr) != 5):
        timeStr = "0"+timeStr

    dateTimeStr = dateStr + " " + timeStr
    if(timeStr.find(":") == -1):
        return datetime.strptime(dateTimeStr, "%d/%m/%Y %H").strftime("%Y-%m-%dT%H:%M:%S")
    else:
        return datetime.strptime(dateTimeStr, "%d/%m/%Y %H:%M").strftime("%Y-%m-%dT%H:%M:%S")


if __name__ == '__main__':
    testShifts = [{'Date': '09/09/2019', 'Time': '16-22:30'}, {'Date': '10/09/2019', 'Time': '9:30-16'}, {'Date': '11/09/2019',
                                                                                                          'Time': '9:30-16'}, {'Date': '14/09/2019', 'Time': '11:30-20'}, {'Date': '15/09/2019', 'Time': '16-22:30'}]

    createShifts(testShifts)
