from googleHelpers import gmailHelper
from googleHelpers import calendarHelper
import json
from pathlib import Path
from datetime import datetime


def main():

    # Load last updated date from config.
    rawConfig = Path("config.json").read_text()
    config = json.loads(rawConfig)
    configLastUpdated = datetime.strptime(config['lastupdated'], "%d/%m/%Y")

    # Compare config last upated date to most recent email date.
    mostRecentUpdatedDate = gmailHelper.getLastUpdatedDate()
    if configLastUpdated >= mostRecentUpdatedDate:
        print("already up to date")
        return
    else:
        configLastUpdated = mostRecentUpdatedDate

    # Parse shifts from email
    shifts = gmailHelper.getShifts()
    # Create events from shifts
    calendarHelper.createShifts(shifts)

    # If everything is successfull update the config last updated time
    config['lastupdated'] = configLastUpdated.strftime("%d/%m/%Y")
    updatedConfig = json.dumps(config)
    Path("config.json").write_text(updatedConfig)


if __name__ == "__main__":
    main()
